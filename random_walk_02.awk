#
#
# Lista de Funções:
#
#
#	01. nl				: Imprime um "new line" no data stream.
#	02. print_param			: Imprime no data stream todos os parâmetros de configuração da Random Walk (RW)
#	03. print_data			: Imprime no data stream uma determinada estrutura de dados/array
#	04. read_data_separator		: Lê o separador de dados usado no arquivo de configuração "config_file"
#	05. read_config_file		: Lê o arquivo de configuração "config_file"
#	06. start_target		: Inicializa a palavra-alvo de acordo com o comprimento definido
#	07. zeros			: Inicializa uma estrutura de dados (array) com zeros e a coloca em contexto numérico
#	08. init_pop			: Inicializa a população da RW
#	09. gauss			: Gera um número pseudo-aleatório gaussiano com média "mu" e desvio-padrão "sigma"
#	10. create_offspring		: Cria a população de filhos via mutação gaussiana
#	11. denormalize			: Desnormaliza o valor dos genes utilizados no cromossomo
#	12. calc_fitness		: Calcula o fitness para uma população de indivíduos
#	13. selection			: Faz a seleção dos invíduos da próxima população no estilo (P1 + P2)-RW (P1 = P2)
#	14. sort_by_fitness		: Ordena a população de acordo com o fitness
#	15. replace_10p_worst		: Substitui os 10% piores indivíduos por indivíduos aleatórios
#	16. elitism			: Implementa o elitismo (mantém sempre o melhor indivíduo na população)
#	17. replace_pop			: Substitui a população atual pela população da próxima geração
#	18. run_my_simulation		: Roda a RW
#
#
#
# Lista de Variáveis:
#
#
#	01. separator_file	: Variável do tipo string que armazena o nome do arquivo contendo o separador de dados usado no arquivo de configuração
#	02. config_file		: Variável do tipo string que armazena o nome do arquivo de configuração
#	03. target_file		: Variável do tipo string que armazena a palavra-alvo
#	04. data_sep		: Variável do tipo string que armazena o separador de dados usado no arquivo de configuração
#	05. tsize		: Variável do tipo numérica que armazena o tamanho da palavra-alvo
#	06. psize		: Variável do tipo numérica que armazena o tamanho da população
#	07. genes		: Variável do tipo numérica que armazena a quantidade de genes do cromossomo
#	08. lbound		: Variável do tipo numérica que armazena o limite inferior do domínio da função fitness
#	09. ubound		: Variável do tipo numérica que armazena o limite superior do domínio da função fitness
#	10. g			: Variável do tipo numérica que armazena a quantidade de gerações da simulação
#	11. mu			: Variável do tipo numérica que armazena o valor da média da função de densidade gaussiana
#	12. sigma		: Variável do tipo numérica que armazena o valor do desvio-padrão da função de densidade gaussiana
#	13. target		: Array do tipo numérico que armazena a palavra-alvo
#	14. pop1		: Array do tipo numérico que armazena a população de pais
#	15. pop2		: Array do tipo numérico que armazena a população intermediária após o torneio estocástico
#	16. pop3		: Array do tipo numérico que armazena a população intermediária após o crossover
#	17. fit1		: Array do tipo numérico que armazena o fitness da população de pais
#	18. fit2		: Array do tipo numérico que armazena o fitness da população intermediária pop2
#	19. fit3		: Array do tipo numérico que armazena o fitness da população intermediária pop3 (usado apenas em testes)
#	20. set_param		: Array do tipo numérico que armazena que atribui aos parâmetros do AG os seus valores
#
#


BEGIN {
	separator_file		= "separator_file.dat";
	config_file		= "config_file_rw.dat";
	target_file		= "palavra_alvo.dat";
	data_sep		= read_data_separator(separator_file);
	tsize			= "tsize";
	psize			= "psize";
	genes			= "genes";
	lbound			= "lbound";
	ubound			= "ubound";
	g			= "g";
	mu			= "mu";
	sigma			= "sigma";
	target[0]		= "";
	pop1[0]			= "";
	pop2[0]			= "";
	pop3[0]			= "";
	fit1[0]			= "";
	fit2[0]			= "";
	fit3[0]			= "";
	bestind[0]		= "";
	bestfit[0]		= "";
	set_param[tsize]	= "";

	srand();
	read_config_file(set_param, config_file, data_sep);

	tsize 	= set_param[tsize];
	psize	= set_param[psize];
	genes	= tsize;
	lbound	= set_param[lbound];
	ubound	= set_param[ubound];
	g	= set_param[g];
	mu	= set_param[mu];
	sigma	= set_param[sigma];

	run_my_simulation();
}

function nl()
{
	printf("\n");
}

function print_param()
{
	print "data_sep  = \""data_sep"\"";
	print "tsize     = ", tsize;
	print "psize     = ", psize;
	print "genes     = ", genes;
	print "lbound    = ", lbound;
	print "ubound    = ", ubound;
	print "g         = ", g;
	print "mu        = ", mu;
	print "sigma     = ", sigma;
}

function print_data(data, nl, nc,     i, k)
{
	for(i = 0; i < nl; i++)
	{
		for(k = 0; k < nc; k++)
			printf("%7.3f ", data[i*nc + k]);
		printf("\n");
	}
}

function read_data_separator(separator_file,     line)
{
	while((getline line < separator_file) > 0)
		return line;
}

function read_config_file(set_param, config_file, data_sep,     i, n, line, store_param)
{
	while((getline line < config_file) > 0)
	{
		gsub("[^A-Za-z0-9."data_sep"]*", "", line);
		n = split(line, store_param, data_sep);
		set_param[store_param[1]] = store_param[2];
	}
	
}

function start_target(target, tsize, target_file,     line, i)
{
	while(((getline line < target_file) > 0) && (i < tsize))
		target[i++] = line;
}

function zeros(mat, ind, genes,     i, k)
{
	for(i = 0; i < ind; i++)
		for(k = 0; k < genes; k++)
			mat[i*genes + k] = 0+0;
}

function init_pop(pop, ind, genes,     i, k)
{
	for(i = 0; i < ind; i++)
		for(k = 0; k < genes; k++)
			pop[i*genes + k] = rand();
}

function gauss(mu, sigma,     sum, n, i, z)
{
	sum = 0;
	n = 12;
	z = 0;

	for(i = 0; i < n; i++)
		sum += rand();
	z = sum - n/2;
	z = mu + sigma*z;
	return (z);
}

function create_offspring(pop1, pop2, ind, genes, mu, sigma,     i, k, aux)
{
	for(i = 0; i < ind; i++)
		for(k = 0; k < genes; k++)
		{
			aux = -1;
			while(aux < 0 || aux > 1)
				aux = pop1[i*genes + k] + gauss(mu, sigma);
			pop2[i*genes + k] = aux;
		}
}

function denormalize(norm, lbound, ubound,     i, k, denorm)
{
	denorm = lbound + (ubound - lbound)*norm;
	return (denorm);
}

function calc_fitness(pop, fit, target, ind, genes, lbound, ubound,     i, k, aux1, aux2)
{
	for(i = 0; i < ind; i++)
	{
		fit[i] = 0;
		aux1 = 0;
		for(k = 0; k < genes; k++)
		{
			aux2 = pop[i*genes + k];
			aux1 = target[k] - denormalize(aux2, lbound, ubound);
			fit[i] += -1*(aux1*aux1);
		}
	}
}

function selection(pop1, pop2, pop3, fit1, fit2, fit3, ind, genes,     i, k)
{
	for(i = 0; i < ind; i++)
		if(fit2[i] > fit1[i])
		{
			fit3[i] = fit2[i];
			for(k = 0; k < genes; k++)
				pop3[i*genes + k] = pop2[i*genes + k];
		}
		else
		{
			fit3[i] = fit1[i];
			for(k = 0; k < genes; k++)
				pop3[i*genes + k] = pop1[i*genes + k];
		}
}

function sort_by_fitness(pop, fit, ind, genes,     popaux, fitaux, sorted, popaux2, vetaux, i , k, n)
{
	for(i = 0; i < ind; i++)
	{
		fitaux[i + 1] = fit[i];
		for(k = 0; k < genes; k++)
			popaux[fitaux[i + 1]] = popaux[fitaux[i + 1]]" "pop[i*genes + k];
	}

	k = asort(fitaux, sorted);

	for(i = k; i > 0; i--)
		popaux2[k - i] = sorted[i]" "popaux[sorted[i]];

	for(i = 0; i < ind; i++)
	{
		n = split(popaux2[i], vetaux, " ");
		fit[i] = vetaux[1];
		for(k = 0; k < n-1; k++)
			pop[i*genes + k] = vetaux[k+2];
	}
}

function replace_10p_worst(pop, ind, genes,     p10, i, k)
{
	p10 = ind/10 < 1 ? 1 : int(ind/10);

	for(i = ind-p10; i < ind; i++)
		for(k = 0; k < genes; k++)
			pop[i*genes + k] = rand();
}

function elitism(pop, fit, bestind, bestfit, ind, genes, g, lbound, ubound,     i)
{
	bestfit[g] = fit[0];
	printf("%d %.6f ", g, bestfit[g]);
	for(i = 0; i < genes; i++)
	{
		bestind[g*genes + i] = pop[i];
		printf("%7.3f ", denormalize(pop[i], lbound, ubound));
	}
	printf("\n");
}

function replace_pop(pop1, pop3, fit1, fit3, ind, genes,     i, k)
{
	for(i = 0; i < ind; i++)
	{
		fit1[i] = fit3[i];
		for(k = 0; k < genes; k++)
			pop1[i*genes + k] = pop3[i*genes + k];
	}
}

function run_my_simulation()
{
	i = 0;
	zeros(pop1, psize, genes);
	zeros(pop2, psize, genes);
	zeros(pop3, psize, genes);
	zeros(fit1, psize, 1);
	zeros(fit2, psize, 1);
	zeros(fit3, psize, 1);
	zeros(bestfit, g, 1);
	zeros(bestind, g, genes);

	start_target(target, tsize, target_file)
	init_pop(pop1, psize, genes);

	while(i < g)
	{
		create_offspring(pop1, pop2, psize, genes, mu, sigma)
		calc_fitness(pop1, fit1, target, psize, genes, lbound, ubound);
		calc_fitness(pop2, fit2, target, psize, genes, lbound, ubound);
		selection(pop1, pop2, pop3, fit1, fit2, fit3, psize, genes);
		sort_by_fitness(pop3, fit3, psize, genes);
		replace_10p_worst(pop3, psize, genes);
		replace_pop(pop1, pop3, fit1, fit3, psize, genes);
		elitism(pop1, fit1, bestind, bestfit, psize, genes, i, lbound, ubound);
		i++;
	}	
}