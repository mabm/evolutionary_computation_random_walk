# README #

### What is this repository for? ###

* **Quick summary**

This README file is about two kinds of optimization algorithms requested during the evolutionary computation
classes: A random walk and a genetic algorithm.

Both algorithms must find a string of floating point numbers. The string's length is user defined.

* **Version**

There are two versions of each algorithm: A preliminary version lacking lots of features requested during classes;
and a final version that is full featured and is according to what was requested.


### How do I get set up? ###

* **Summary of set up**

The set up for both the **Random Walk** and the **Genetic Algorithm** is very straightforward. The user needs just a
working version of GAWK (GNU's AWK), since both codes are written in the AWK programming language.

* **Configuration**

The configuration is done through a configuration file called "*config_file*". This file stores all the parameters
needed for the simulation:

01. Target size (tsize)
02. Population size (psize)
03. Fitness function's lower bound (lbound)
04. Fitness function's upper bound (ubound)
05. Iteration/Generations (g)
06. The gaussian density function's average (mu)
07. The gaussian density function's standard deviation (sigma)
08. Probability of corssover (pc, genetic algorithm only)
09. Probability of mutation (pm, genetic algorithm only)
10. Stochastic tournament size (ts, genetic algorithm only)

The "*config_file_ga.dat*" stores all the parameters for the genetic algorithm and the "*config_file_rw.dat*' does
the same for the random walk.

* **Dependencies**

As aforementioned, there are no dependencies and both algorithms are self-contained codes, needing no additional
classes, packages, libraries and so on. The user just needs a working GAWK interpreter to run both codes.

* **How to run tests**

In order to test the code the user needs to type on the command line this simple command:

awk -f "FILE.awk"

Where "FILE.awk" is the corresponding file for one of the codes provided here in this repository.

### Contribution guidelines ###

* **Writing tests**

Tests can be written according to the demands of a given situation and/or user.

* **Code review**

Anyone minimally fluent in the AWK programming language can and **should** review the codes.


* **Other guidelines**

There are no other guidelines. :-)

### Who do I talk to? ###

* **Repo owner or admin**

This repository belongs to Marcelo Augusto de Brito Mendes.

* **Other community or team contact**

The owner can be contacted right here on BitBucket.org..