BEGIN {
#	Lista de Funções:
#
#		01. zeros:		Inicializa uma estrutura de dados com valores numéricos nulos
#
#
	config_file = "config_file.dat"		# Arquivo contendo todos os parâmetros da simulação
	target_file = "palavra_alvo.dat";	# Arquivo contendo a palavra-alvo
	tsize	= 5;				# Tamanho da palavra alvo
	psize	= 5;				# Tamanho da população
	genes	= tsize;			# Quantidante de genes/variáveis em cada cromossomo
	ts	= 3;
	lbound	= 50;				# Limite inferior do intervalo das variáveis
	ubound	= 130;				# Limite superior do intervalo das variáveis
	g = 1;
	target[0]	= "";			# Palavra-alvo
	pop1[0] = "";				# Matriz contendo os pais da população
	pop2[0] = "";				# Matriz contendo os filhos da população
	pop3[0] = "";				# Matriz temporária usada no mecanismo de seleção
	fit1[0]	= "";				# Vetor coluna contendo o fitness da população de pais
	fit2[0] = "";				# Vetor coluna contendo o fitness da população de filhos
	srand(0);
	run_my_simulation(g);
}

function nl()
{
	printf("\n");
}

function start_target(target, tsize, target_file,     i, x)
{
	while(((getline x < target_file) > 0) && (i < tsize))
		target[i++] = x;
}

function zeros(mat, ind, genes,     i, k)
{
	for(i = 0; i < ind; i++)
		for(k = 0; k < genes; k++)
			mat[i*genes+k] = 0+0;
}

function print_data(data, line, col,     i, k)
{
	for(i = 0; i < line; i++)
	{
		for(k = 0; k < col; k++)
			printf("%7.3f ", data[i*col+k]);
		printf("\n");
	}
}

function gauss(mu, sigma,     sum, i, z, n)
{
	sum = 0;
	n = 12;
	for(i = 0; i < n; i++)
		sum += rand();
	z = sum - n/2;
	return (z);
}

function init_pop(mat, ind, genes,     i, k)
{
	for(i = 0; i < ind; i++)
		for(k = 0; k < genes; k++)
			mat[i*genes+k] = rand();
			
}

function create_offspring(pop1, pop2, ind, genes,     i, k)
{
	for(i = 0; i < ind; i++)
		for(k = 0; k < genes; k++)
		{
			pop2[i*genes+k] = -1;
			while(pop2[i*genes+k] < 0 || pop2[i*genes+k] > 1)
				pop2[i*genes+k] = pop1[i*genes+k] + gauss(0, 1);
		}
}

function denormalize(norm,     lower, upper, denorm)
{
	lower = 50;
	upper = 130;
	denorm = lower + (upper - lower)*norm;
	return (denorm);
}

function calc_fitness(pop, fit, ind, genes,     i, k, x)
{
	for(i = 0; i < ind; i++)
	{
		fit[i] = 0;
		x = 0;
		for(k = 0; k < genes; k++)
		{
			x = target[i*genes+k] - denormalize(pop[i*genes+k]);
			fit[i] += x*x;
		}
	}
}

function stochastic_tournament(pop1, pop2, fit1, fit2, ind, genes, ts,     i, a, k, tind, best, besti, x)
{
	for(i = 0; i < ind; i++)
	{
		best = 10000000;
		for(a = 0; a < ts; a++)
			tind[a] = int(ind*rand());
#		print_data(tind, ts, 1);
		for(a = 0; a < ts; a++)
		{
			x = tind[a];
#			printf("X = %d | fit[%d] = %f\n", x, x, fit1[x]);
			if(fit1[x] < best)
			{
				best = fit1[x];
				besti = x;
			}
		}
#		printf("besti = %d | fit[%d] = %f\n\n", besti, besti, best);
#		printf("\n\n%.3f ", fit1[besti]);
		for(k = 0; k < genes; k++)
		{
			pop2[i*genes+k] = pop1[besti*genes+k];
#			printf("%7.3f ", pop2[i*genes+k]);
		}
#		printf("\n\n");
		fit2[i] = fit1[besti];
	}
}

function crossover(pop, popx, ind, genes,     i, k, c1, c2)
{
	for(i = 0; i < ind; i++)
	{
		c1 = int(ind*rand());
		c2 = int(ind*rand());
		for(k = 0; k < genes; k++)
		{
			x = -1;
			while(x < 0 || x > 1)
				x = pop[c1*genes+k] + (pop[c1*genes+k] - pop[c2*genes+k])*rand();
			popx[i*genes+k] = x;
		}
	}
	print_data(popx, ind, genes);
}

function mutation(pop, ind, genes,     i, k)
{
	
}

function run_my_simulation(g,     i)
{
	zeros(target, 1, genes);
	zeros(pop1, psize, genes);
	zeros(pop2, psize, genes);
	zeros(pop3, psize, genes);
	zeros(fit1, psize, 1);
	zeros(fit2, psize, 1);
	zeros(fit3, psize, 1);

	start_target(target, tsize, target_file);
	init_pop(pop1, psize, genes);

	while(i++ < g)
	{
		calc_fitness(pop1, fit1, psize, genes);
		print_data(pop1, psize, genes);
		printf("\n");
		print_data(fit1, psize, 1);
		stochastic_tournament(pop1, pop2, fit1, fit2, psize, genes, ts);
		print_data(pop2, psize, genes);
		calc_fitness(pop2, fit2, psize, genes);
		print_data(fit2, psize, 1);
		printf("\n");
		crossover(pop2, pop3, psize, genes);
	}
}

