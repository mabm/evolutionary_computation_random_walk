BEGIN {
#	Lista de Funções:
#
#		01. zeros:		Inicializa uma estrutura de dados com valores numéricos nulos
#
#
	config_file = "config_file.dat"		# Arquivo contendo todos os parâmetros da simulação
	target_file = "palavra_alvo.dat";	# Arquivo contendo a palavra-alvo
	tsize	= 5;				# Tamanho da palavra alvo
	psize	= 50;				# Tamanho da população
	genes	= tsize;			# Quantidante de genes/variáveis em cada cromossomo
	lbound	= 50;				# Limite inferior do intervalo das variáveis
	ubound	= 130;				# Limite superior do intervalo das variáveis
	target[0]	= "";			# Palavra-alvo
	pop1[0] = "";				# Matriz contendo os pais da população
	pop2[0] = "";				# Matriz contendo os filhos da população
	pop3[0] = "";				# Matriz temporária usada no mecanismo de seleção
	fit1[0]	= "";				# Vetor coluna contendo o fitness da população de pais
	fit2[0] = "";				# Vetor coluna contendo o fitness da população de filhos
	fit3[0] = "";
	srand(0);

	start_target(target, tsize, target_file);
	print_data(target, 1, genes);
	nl();

	zeros(pop1, psize, genes);
	print_data(pop1, psize, genes);
	nl();

	zeros(pop2, psize, genes);
	print_data(pop2, psize, genes);
	nl();

	zeros(fit1, psize, 1);
	zeros(fit2, psize, 1);
	zeros(pop2, psize, genes);

	init_pop(pop1, psize, genes);
	print_data(pop1, psize, genes);
	nl();

	create_offspring(pop1, pop2, psize, genes);
	print_data(pop2, psize, genes);
	nl();

	calc_fitness(pop1, fit1, psize, genes);
	print_data(fit1, psize, 1);
	nl();

	calc_fitness(pop2, fit2, psize, genes);
	print_data(fit2, psize, 1);
	nl();

	selection(pop1, pop2, pop3, fit1, fit2, fit3, psize, genes);
	print_data(pop3, psize, genes);
	nl();
	print_data(fit3, psize, 1);

	replace_10p_worst(pop3, fit3, psize, genes);
	print_data(pop3, psize, genes);
	nl();

	new_pop1(pop1, pop3, psize, genes);
	print_data(pop1, psize, genes);
	nl();
}

function nl()
{
	printf("\n");
}

function start_target(target, tsize, target_file,     i, x)
{
	while(((getline x < target_file) > 0) && (i < tsize))
		target[i++] = x;
}

function zeros(mat, ind, genes,     i, k)
{
	for(i = 0; i < ind; i++)
		for(k = 0; k < genes; k++)
			mat[i*genes+k] = 0+0;
}

function print_data(data, line, col,     i, k)
{
	for(i = 0; i < line; i++)
	{
		for(k = 0; k < col; k++)
			printf("%7.3f ", data[i*col+k]);
		printf("\n");
	}
}

function gauss(mu, sigma,     sum, i, z, n)
{
	sum = 0;
	n = 12;
	for(i = 0; i < n; i++)
		sum += rand();
	z = sum - n/2;
	return (z);
}

function init_pop(mat, ind, genes,     i, k)
{
	for(i = 0; i < ind; i++)
		for(k = 0; k < genes; k++)
			mat[i*genes+k] = rand();
			
}

function create_offspring(pop1, pop2, ind, genes,     i, k)
{
	for(i = 0; i < ind; i++)
		for(k = 0; k < genes; k++)
		{
			pop2[i*genes+k] = -1;
			while(pop2[i*genes+k] < 0 || pop2[i*genes+k] > 1)
				pop2[i*genes+k] = pop1[i*genes+k] + gauss(0, 1);
		}
}

function denormalize(norm,     lower, upper, denorm)
{
	lower = 50;
	upper = 130;
	denorm = lower + (upper - lower)*norm;
	return (denorm);
}

function calc_fitness(pop, fit, ind, genes,     i, k, x)
{
	for(i = 0; i < ind; i++)
	{
		fit[i] = 0;
		x = 0;
		for(k = 0; k < genes; k++)
		{
			x = target[k] - denormalize(pop[i*genes+k]);
			fit[i] += -(x*x);
		}
	}
}

function selection(pop1, pop2, pop3, fit1, fit2, fit3, ind, genes,     i, k)
{
	for(i = 0; i < ind; i++)
		if(fit2[i] > fit1[i])
		{
			fit3[i] = fit2[i];
			for(k = 0; k < genes; k++)
				pop3[i*genes+k] = pop2[i*genes+k];
		}
		else
		{
			fit3[i] = fit1[i];
			for(k = 0; k < genes; k++)
				pop3[i*genes+k] = pop1[i*genes+k];
		}
}

function replace_10p_worst(pop, fit, ind, genes,     i, k, a, p10, file1, file2, x, worsti)
{
	file1 = "tmp01.dat";
	file2 = "tmp02.dat";
	a = 0;
	p10 = psize/10 < 1 ? 1 : int(psize/10);

	for(i = 0; i < ind; i++)
		printf("%d %7.3f\n", i, fit[i]) > "tmp01.dat";
	system("sort -nrk2,2 tmp01.dat | awk '{$2 = \"\"; print;}' > \"tmp02.dat\"");
#	system("sort -nrk2,2 tmp01.dat");

	for(i = 0; i < p10; i++)
		getline worsti[i] < file2;

	for(i = 0; i < p10; i++)
		print "############", worsti[i];

	for(i = 0; i < p10; i++)
	{
		x = worsti[i];
		print "x = ", x;
		for(k = 0; k < genes; k++)
			pop[x*genes+k] = rand();
	}
}

function new_pop1(pop1, pop3, ind, genes,     i, k)
{
	for(i = 0; i < ind; i++)
		for(k = 0; k < genes; k++)
			pop1[i*genes+k] = pop3[i*genes+k];
}

function print_info(pop, fit, ind, genes, g,     i, k)
{
	for(i = 0; i < ind; i++)
	{
		printf("%d %d %7.3f ", g, i, fit[i]);
		for(k = 0; k < genes; k++)
			printf("%7.3f ", pop[i*ind+k]);
		printf("\n");
	}
}

function run_my_simulation(g,     i)
{
	zeros(target, 1, genes);
	zeros(pop1, psize, genes);
	zeros(pop2, psize, genes);
	zeros(pop3, psize, genes);
	zeros(fit1, psize, 1);
	zeros(fit2, psize, 1);
	zeros(fit3, psize, 1);

	start_target(target, tsize, target_file);
	init_pop(pop1, psize, genes);
	srand(0);

	while(i++ < g)
	{
		calc_fitness(pop1, fit1, psize, genes);
		create_offspring(pop1, pop2, psize, genes);
		calc_fitness(pop2, fit2, psize, genes);
		selection(pop1, pop2, pop3, fit1, fit2, fit3, psize, genes);
#		replace_10p_worst(pop3, fit3, psize, genes);
		new_pop1(pop1, pop3, psize, genes);
	}
}

